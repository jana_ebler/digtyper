/* Copyright 2015 Jana Ebler
 *
 * This file is part of DIGTYPER.
 *
 * DIGTYPER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DIGTYPER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with genotyperINVDUP  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef READALIGNMENT_H
#define READALIGNMENT_H

#include <bamtools/api/BamAux.h>
#include <bamtools/api/BamAlignment.h>
#include <bamtools/api/BamWriter.h>
#include <bamtools/api/BamReader.h>

#include <string>

class ReadAlignment
{
	public:
		//constructor
		ReadAlignment();

		ReadAlignment(std::string name, BamTools::BamAlignment& aln1, BamTools::BamAlignment& aln2, bool leftSet, bool rightSet);

		void setName(std::string name);

		void setLeft(BamTools::BamAlignment& aln1);

		void setRight(BamTools::BamAlignment& aln2);

		std::string getName();

		BamTools::BamAlignment& getLeft();

		BamTools::BamAlignment& getRight();

	private:

		std::string name;
		
		//the alignment of the left read end
		BamTools::BamAlignment left;

		//the alignment of the right read end
		BamTools::BamAlignment right;

	public:		
		bool leftSet;
		bool rightSet;
};

#endif // READALIGNMENT_H
