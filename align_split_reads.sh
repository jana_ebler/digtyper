#!/bin/bash

set -e

input_bamfile=$1
input_reference=$2
span=$3

bwa index $input_reference

cat ${input_bamfile}.bam | clever-toolkit/src/extract-bad-reads -a -l 35 --threads 20 -S ${input_bamfile}.split.fastq ${input_bamfile}.1.fastq ${input_bamfile}.2.fastq
bwa aln -t 20  $input_reference ${input_bamfile}.split.fastq > ${input_bamfile}.split.sai
bwa samse -n 25 $input_reference ${input_bamfile}.split.sai ${input_bamfile}.split.fastq | xa2multi.pl | samtools view -Sb -> ${input_bamfile}.split.bwa.bam

rm ${input_bamfile}.split.fastq ${input_bamfile}.split.sai

cat ${input_bamfile}.split.bwa.bam | clever-toolkit/src/laser-core --soft_clip --m_in_cigar --max_span $span --threads 20 --inversions -I $input_reference ${input_bamfile}.1.fastq ${input_bamfile}.2.fastq > ${input_bamfile}.laser.bam

#remove files that are not needed anymore
rm ${input_bamfile}.1.fastq ${input_bamfile}.2.fastq ${input_bamfile}.split.bwa.bam

samtools sort ${input_bamfile}.laser.bam ${input_bamfile}.sorted_laser

rm ${input_bamfile}.laser.bam

samtools index ${input_bamfile}.sorted_laser.bam

