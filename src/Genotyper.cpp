/* Copyright 2015 Jana Ebler
 *
 * This file is part of DIGTYPER.
 *
 * DIGTYPER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DIGTYPER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with genotyperINVDUP  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Genotyper.h"
#include "ReadAlignment.h"
#include "ReadBam.h"
#include <vector>
#include <algorithm>
#include <boost/unordered_set.hpp>
#include <boost/math/distributions.hpp>
#include <bamtools/api/BamAlignment.h>
#include <math.h>

Genotyper::Genotyper(double mqt,int bam_window_s, bool reversed, bool split, bool print_IV, int split_acc)
	:map_quality_threshold(mqt),
	 bam_window_size(bam_window_s),
	 use_reversed(reversed),
	 use_split(split),
	 print_IV(print_IV),
	 split_acc(split_acc)
{}

std::pair<int,int> Genotyper::getSplitPos(BamTools::BamAlignment& read)
{	
	std::string split_positions = "";
	if(!read.GetTag("IV", split_positions))
	{
		std::cerr << "error tag could not be read" << std::endl;
		return std::make_pair(-1,-1);
	}

	//filter the positions
	std::size_t found = split_positions.find_first_of(";");
	int pos1 = atoi(split_positions.substr(0,found).c_str());
	int pos2 = atoi(split_positions.substr(found+1,split_positions.size()).c_str());

	return std::make_pair(std::min(pos1,pos2), std::max(pos1,pos2));
}

void Genotyper::computeLeftAndRight(ReadAlignment& read, BamTools::BamAlignment& left, BamTools::BamAlignment& right)
{
	if(read.getLeft().Position <= read.getRight().Position)
	{
		left = read.getLeft();
		right = read.getRight();
	} else {
		left = read.getRight();
		right = read.getLeft();
	}
}

void Genotyper::compute_stats_inv(Genotyper::variation_stats_t& result, BamTools::BamReader& reader, Variation& var)
{	
	int ref_id = reader.GetReferenceID(var.chromosome);

	if(ref_id < 0)
	{
		std::cerr << "reference id < 0 " << std::endl;
		return;
	}

	if(var.type != Variation::INVERSION)
	{
		std::cerr << "other variation than inversion." << std::endl;
		return;
	}

	//store used reads
	boost::unordered_set<std::string> usedReads;

	//region to consider
	int start = std::max(0, (int)(var.coord1 - bam_window_size));
	int end = var.coord2 + bam_window_size;

	//get all alignments from this region
	ReadBam bam_helper;	
	std::vector<ReadAlignment> aln_pairs;
	
	try
	{
		bam_helper.readFromRegion(reader, aln_pairs, ref_id, start, end);
	}
	catch(std::runtime_error e)
	{
		std::cerr << e.what() << std::endl;
	}

	if(use_reversed)
	{
		//now iterate over all these read-alignments
		for(size_t i = 0; i < aln_pairs.size(); i++)
		{
			//check if read was used before
			boost::unordered_set<std::string>::iterator it = usedReads.find(aln_pairs[i].getName());
			if(it != usedReads.end())
			{
				continue;
			}

			bool read_used = false;

			//look for left and right alignments
			BamTools::BamAlignment left;
			BamTools::BamAlignment right;
			computeLeftAndRight(aln_pairs[i], left, right);

			BamTools::BamAlignment r;
			bool isLeft = true;		
			
			if(left.HasTag("IV") || right.HasTag("IV")) continue;

			//check if one of the read ends is in between the breakpoint and the other one not
			if(var.isBetween(left))
			{
				//if both ends are in between breakpoints, nothing to consider by now
				if(var.isBetween(right)) continue;

				//check if right end is completely outside the borders (no overlapping)
				if(right.Position < var.coord2) continue;

				//check softclipping at right end of left read
				/**if(left.CigarData[left.CigarData.size()-1].Type == 'S')
				{
					if(left.HasTag("IV"))
					{
						//softclipped end streches across breakpoints of variation
						continue;
					}
				}

				if(left.CigarData[0].Type == 'S')
				{
					if(left.HasTag("IV"))
					{
						//softclipped end stetches across breakpoints of variation
						continue;
					}
				}**/
				if(!right.IsReverseStrand()) continue;
				r = left;

			} else {
				//none of the reads is in between
				if(!var.isBetween(right)) continue;

				//check if left end is completely outside the borders (no overlapping)
				if(left.GetEndPosition() > var.coord1) continue;

				//check softclipping at left end of right read
				/*if(right.CigarData[0].Type == 'S')
				{
					if( right.HasTag("IV"))
					{
						continue;
					}
				}

				if(right.CigarData[right.CigarData.size()-1].Type == 'S')
				{
					if(right.HasTag("IV"))
					{
						continue;
					}
				}*/
				if(left.IsReverseStrand()) continue;
				r = right;
				isLeft = false;
			}

			//check the mapping quality
			if(r.MapQuality < map_quality_threshold)
			{
				continue;
			}
	
			read_used = true;

			//test if orientation of the read end is reversed 
			bool reversed = false;
			
			if(isLeft)
			{
				if(r.IsReverseStrand()) reversed = true;
			} else {
				if(!r.IsReverseStrand()) reversed = true; 
			}

			//compute probability that alignment is wrong
			double p_wrong1 = pow(10.0,-((double)left.MapQuality)/10.0);
			double p_wrong2 = pow(10.0,-((double)right.MapQuality)/10.0);
			double p_wrong = std::max(0.05,p_wrong1*p_wrong2 + (1.0-p_wrong1)*p_wrong2 + p_wrong1*(1.0-p_wrong2));

			//compute probability triple
			if(!reversed)
			{	
				//regular read
				result.ref_support_rev += 1;
				result.read_probs.push_back(GenotypeDist(2.0/3.0-1.0/3.0*p_wrong, 1.0/3.0, 1.0/3.0*p_wrong));	

			} else {
			
				//reversed read
				result.alt_support_rev += 1;
				result.read_probs.push_back(GenotypeDist(1.0/3.0*p_wrong, 1.0/3.0, 2.0/3.0-1.0/3.0*p_wrong));
			}

			if(read_used)
			{
				usedReads.insert(aln_pairs[i].getName());
			}	
		}
	}
	
	if(use_split)
	{
		for(size_t i = 0; i < aln_pairs.size(); i++)
		{
			//check if read was used before
			boost::unordered_set<std::string>::iterator it = usedReads.find(aln_pairs[i].getName());
			if(it != usedReads.end())
			{
				continue;
			}
		
			//look for left and right alignments
			BamTools::BamAlignment left;
			BamTools::BamAlignment right;
			computeLeftAndRight(aln_pairs[i], left, right);

			bool supporting = false;
			double p_wrong;

			if(left.HasTag("IV"))
			{
				std::pair<int,int> pos = getSplitPos(left);
				if(pos.first == -1){continue;}
				if(print_IV) std::cout << var.chromosome << '\t' << var.coord1 << '\t' << var.coord2 << '\t' << pos.first << '\t' << pos.second << std::endl;

				if(right.HasTag("IV") || var.strechesOver(right,true))
				{
					continue;
				}
				
				if(left.MapQuality < map_quality_threshold)
				{
					continue;
				}

				if((std::abs(pos.first - (int)var.coord1) <= split_acc) && (std::abs(pos.second - (int)var.coord2) <= split_acc))
				{
						//read supports the inversion
						supporting = true;
				} else { continue;}

				p_wrong = std::max(0.05,pow(10.0,-((double)left.MapQuality)/10.0));

			} else if(right.HasTag("IV"))
			{

				std::pair<int,int> pos = getSplitPos(right);
                                if(pos.first == -1){continue;}
                                if(print_IV) std::cout << var.chromosome << '\t' << var.coord1 << '\t' << var.coord2 << '\t' << pos.first << '\t' << pos.second << std::endl;

				
				if(left.HasTag("IV") || var.strechesOver(left,false))
				{
					continue;
				}

				if(right.MapQuality < map_quality_threshold)
				{
					continue;
				}

				if((std::abs(pos.first - (int)var.coord1) <= split_acc) && (std::abs(pos.second - (int)var.coord2) <= split_acc))
				{
						//read supports the inversion
						supporting = true;		
				} else {continue;} 
		
				p_wrong = std::max(0.05,pow(10.0,-((double)right.MapQuality)/10.0));

			} else {

				if(!((var.strechesOver(left,false) && !var.strechesOver(right,true)) || (var.strechesOver(left, true) && !var.isBetween(right)) || (var.strechesOver(right,false) && !var.isBetween(left)) || (var.strechesOver(right, true) && !var.strechesOver(left,false))))
				{
					continue;
				} else {
				
					//read is regular
					if(var.strechesOver(left, true) || var.strechesOver(left,false))
					{
						p_wrong = std::max(0.05,pow(10.0,-((double)left.MapQuality)/10.0));
					} else {
						p_wrong = std::max(0.05,pow(10.0,-((double)right.MapQuality)/10.0));		
					}
				}
			}
			
			if(supporting)
			{
				//split read
				result.alt_support_split += 1;
				result.read_probs.push_back(GenotypeDist(1.0/3.0*p_wrong, 1.0/3.0, 2.0/3.0-1.0/3.0*p_wrong));

			} else {
				//regular read
				result.ref_support_split += 1;
				result.read_probs.push_back(GenotypeDist(2.0/3.0-1.0/3.0*p_wrong, 1.0/3.0, 1.0/3.0*p_wrong));	
			}
	
			//add read to usedReads
			usedReads.insert(aln_pairs[i].getName());
		}
	}	
}

void Genotyper::compute_stats_dup_long(Genotyper::variation_stats_t& result, BamTools::BamReader& reader, Variation& var, double isize_stddev, double isize_mean)
{
	int ref_id = reader.GetReferenceID(var.chromosome);

	if(ref_id < 0)
	{
		std::cerr << "reference id < 0 " << std::endl;
		return;
	}

	if(var.type != Variation::DUPTAN)
	{
		std::cerr << "other variation than duplication." << std::endl;
		return;
	}

	//region to consider
	int start = std::max(0, (int)(var.coord1 - bam_window_size));
	int end = var.coord2 + bam_window_size;

	//get all alignments from this region
	ReadBam bam_helper;	
	std::vector<ReadAlignment> aln_pairs;
	
	try
	{
		bam_helper.readFromRegion(reader, aln_pairs, ref_id, start, end);
	}
	catch(std::runtime_error e)
	{
		std::cerr << e.what() << std::endl;
	}

	//iterate over all alingments to find supporting and neutral reads
	for(size_t i = 0; i < aln_pairs.size(); i++)
	{
		//compute left and right alignment
		BamTools::BamAlignment left;
		BamTools::BamAlignment right;
		computeLeftAndRight(aln_pairs[i], left, right);
		bool countTwice = false;
		int insert_size = 0;

		//check the mapping quality
	        if((right.MapQuality < map_quality_threshold) || (left.MapQuality < map_quality_threshold))
	        {
        		continue;
        	}

		bool leftInside = false;
		bool rightInside = false;

		//check if ends are located in between the duplication breakpoints
		if(var.isBetween(left))
		{
			leftInside = true;

			 //check softclipping at right end of left read
			if(left.CigarData[left.CigarData.size()-1].Type == 'S')
			{
				if(((left.CigarData[left.CigarData.size()-1].Length + left.GetEndPosition()) > var.coord2))
				{
					//softclipped end streches across breakpoints of variation
					leftInside = false;
				}
			}

			if(left.CigarData[0].Type == 'S')
			{
				if(((left.Position - left.CigarData[0].Length) < var.coord1))
				{
					//softclipped end stetches across breakpoints of variation
					leftInside = false;
				}
			}
		}
		
		if(var.isBetween(right))
		{
			rightInside = true;

			//check softclipping at left end of right read
			if(right.CigarData[0].Type == 'S')
			{
				if(((right.Position - right.CigarData[0].Length) < var.coord1))
				{
					//softclipped end stetches across breakpoints of variation
					rightInside = false;
				}
			}

			if(right.CigarData[right.CigarData.size()-1].Type == 'S')
			{
				if(((right.CigarData[right.CigarData.size()-1].Length + right.GetEndPosition()) > var.coord2))
				{
					//softclipped end streches across breakpoints of variation
					rightInside = false;
				}
			}	
		}
		
		if((left.IsReverseStrand()) && (!right.IsReverseStrand()))
		{
			if(leftInside && rightInside)
			{	
				//supports the duplication
				insert_size = left.Position - right.GetEndPosition();

				if((insert_size > (isize_mean-var.computeLength())+3*isize_stddev) || (insert_size < (isize_mean-var.computeLength())-3*isize_stddev))
					continue;

				result.alt_support_insert += 2;
				countTwice = true;
				
			} else { 
				continue; 
				}

		} else if((left.GetEndPosition() > right.Position) && (left.GetEndPosition() < right.GetEndPosition()))
		{
			if(leftInside && rightInside)
			{	
				//supports the duplication
				insert_size = right.Position - left.GetEndPosition();

				if((insert_size > (isize_mean-var.computeLength())+3*isize_stddev) || (insert_size < (isize_mean-var.computeLength())-3*isize_stddev))
                                        continue;				

				result.alt_support_insert += 2;
				countTwice = true;
								
			} else {
				continue;
			}
		
		} else if(leftInside && !rightInside)
		{
			//check if right end is completely outside the breakpoints and not just overlapping
			if(right.Position >= var.coord2)
			{
				//a neutral read (at right end)
				result.ref_support_insert += 1;
				insert_size = right.Position - left.GetEndPosition();

				if(insert_size > (isize_mean+3*isize_stddev))
					continue;
				
			} else { 
				continue;
			}

		} else if(rightInside && !leftInside)
		{
			//check if left end is completely outside the breakpoints and not just overlapping
			if(left.GetEndPosition() <= var.coord1)
			{
				//neutral read (at left end)
				result.ref_support_insert += 1;
				insert_size = right.Position - left.GetEndPosition();

				if(insert_size > (isize_mean+3*isize_stddev))
					continue;

			} else { 
				continue;
			}

		} else { 
			continue;
			}

		int length = var.computeLength();

		//compute probability using the insert size
		boost::math::normal_distribution<> normal(0.0, isize_stddev);
		double val_normal = boost::math::pdf(normal, insert_size-isize_mean);
		double dupl = boost::math::pdf(normal, insert_size-isize_mean+length);
		double sum = 1.0*val_normal+(2.0/3.0)*val_normal+(1.0/3.0)*dupl+(1.0/2.0)*val_normal+(1.0/2.0)*dupl;
		double p_wrong1 = pow(10.0,-((double)left.MapQuality)/10.0);
		double p_wrong2 = pow(10.0,-((double)right.MapQuality)/10.0);
		double pwrong = std::max(0.05,p_wrong1*p_wrong2 + (1.0-p_wrong1)*p_wrong2 + p_wrong1*(1.0-p_wrong2));
		
		double abs = 1.0*val_normal;
		double het = (2.0/3.0)*val_normal + (1.0/3.0)*dupl;
		double hom = (1.0/2.0)*val_normal + (1.0/2.0)*dupl; 

		result.read_probs.push_back(GenotypeDist((1-pwrong)*(abs/sum)+(1.0/3.0)*pwrong,(1-pwrong)*(het/sum)+(1.0/3.0)*pwrong,(1-pwrong)*(hom/sum)+(1.0/3.0)*pwrong));
		if(countTwice) result.read_probs.push_back(GenotypeDist((1-pwrong)*(abs/sum)+(1.0/3.0)*pwrong,(1-pwrong)*(het/sum)+(1.0/3.0)*pwrong,(1-pwrong)*(hom/sum)+(1.0/3.0)*pwrong));
	}
}

void Genotyper::compute_stats_dup_short(Genotyper::variation_stats_t& result, BamTools::BamReader& reader, Variation& var, double isize_stddev, double isize_mean)
{
	int ref_id = reader.GetReferenceID(var.chromosome);

	if(ref_id < 0)
	{
		std::cerr << "reference id < 0 " << std::endl;
		return;
	}

	if(var.type != Variation::DUPTAN)
	{
		std::cerr << "other variation than duplication." << std::endl;
		return;
	}

	//region to consider
	int start = std::max(0, (int)(var.coord1 - bam_window_size));
	int end = var.coord2 + bam_window_size;

	//get all alignments from this region
	ReadBam bam_helper;	
	std::vector<ReadAlignment> aln_pairs;
	
	try
	{
		bam_helper.readFromRegion(reader, aln_pairs, ref_id, start, end);
	}
	catch(std::runtime_error e)
	{
		std::cerr << e.what() << std::endl;
	}

	//iterate over all alingments to find supporting and neutral reads
	for(size_t i = 0; i < aln_pairs.size(); i++)
	{
		//compute left and right alignment
		BamTools::BamAlignment left;
		BamTools::BamAlignment right;
		computeLeftAndRight(aln_pairs[i], left, right);

		//check the mapping quality
		if((right.MapQuality < map_quality_threshold) || (left.MapQuality < map_quality_threshold))
		{
			continue;
		}

		bool leftInside = false;
		bool rightInside = false;
		bool leftSoftClipped = false;
		bool rightSoftClipped = false;

		if(var.isBetween(left))
		{
			//check softclipping at right end of left read
			if(left.CigarData[left.CigarData.size()-1].Type == 'S')
			{
				if(((left.CigarData[left.CigarData.size()-1].Length + left.GetEndPosition()) > var.coord2))
				{
					//softclipped end streches across breakpoints of variation
					leftSoftClipped = true;
				}
			}

				if(left.CigarData[0].Type == 'S')
				{
					if(((left.Position - left.CigarData[0].Length) < var.coord1))
					{
						//softclipped end stetches across breakpoints of variation
						leftSoftClipped = true;
					}
				}

			leftInside = true;
		}
		
		if(var.isBetween(right))
		{
			//check softclipping at left end of right read
			if(right.CigarData[0].Type == 'S')
			{
				if(((right.Position - right.CigarData[0].Length) < var.coord1))
				{
					//softclipped end stetches across breakpoints of variation
					rightSoftClipped = true;
				}
			}

			if(right.CigarData[right.CigarData.size()-1].Type == 'S')
			{
				if(((right.CigarData[right.CigarData.size()-1].Length + right.GetEndPosition()) > var.coord2))
				{
					//softclipped end streches across breakpoints of variation
					rightSoftClipped = true;
				}
			}

			rightInside = true;
		}

		//at least one end must be completely inside the breakpoints
		if(!((leftInside && !leftSoftClipped) || (rightInside && !rightSoftClipped))) continue;
		bool countTwice = false;
		if((leftInside && rightInside) && (!leftSoftClipped && !rightSoftClipped)) countTwice = true;

		int insert_size = right.Position - left.GetEndPosition();

		if((left.IsReverseStrand()) && (!right.IsReverseStrand()))
		{
			insert_size = left.Position - right.GetEndPosition();
		}

		int length = var.computeLength();

		//count supporting and neutral reads
		if(insert_size - isize_mean <= -length/2.0) 
		{
			result.alt_support_insert += 1;
			
			if(countTwice) result.alt_support_insert += 1;
			
		} else {
			result.ref_support_insert += 1;
		}	

		if(insert_size > isize_mean+3*isize_stddev)
			continue;

		//compute probability using the insert size
		boost::math::normal_distribution<> normal(0.0, isize_stddev);
		long double val_normal = boost::math::pdf(normal, insert_size-isize_mean);
		long double dupl = boost::math::pdf(normal, insert_size-isize_mean+length);
		long double sum = 1.0*val_normal+(2.0/3.0)*val_normal+(1.0/3.0)*dupl+(1.0/2.0)*val_normal+(1.0/2.0)*dupl;

		double p_wrong1 = pow(10.0,-((double)left.MapQuality)/10.0);
		double p_wrong2 = pow(10.0,-((double)right.MapQuality)/10.0);
		double pwrong = std::max(0.05,p_wrong1*p_wrong2 + (1.0-p_wrong1)*p_wrong2 + p_wrong1*(1.0-p_wrong2));

		long double abs = 1.0*val_normal;
		long double het = (2.0/3.0)*val_normal + (1.0/3.0)*dupl;
		long double hom = (1.0/2.0)*val_normal + (1.0/2.0)*dupl;

		result.read_probs.push_back(GenotypeDist((1-pwrong)*(abs/sum)+(1.0/3.0)*pwrong,(1-pwrong)*(het/sum)+(1.0/3.0)*pwrong,(1-pwrong)*(hom/sum)+(1.0/3.0)*pwrong));
		if(countTwice) result.read_probs.push_back(GenotypeDist((1-pwrong)*(abs/sum)+(1.0/3.0)*pwrong,(1-pwrong)*(het/sum)+(1.0/3.0)*pwrong,(1-pwrong)*(hom/sum)+(1.0/3.0)*pwrong)); 
	}
}

GenotypeDist Genotyper::computeGenotype(Genotyper::variation_stats_t& result)
{
	//multiply components (-> ml function)
	GenotypeDist r;
	
	for(size_t i = 0; i < result.read_probs.size(); i++)
	{
		r = r*result.read_probs[i];
	}
	
	return r;
}
