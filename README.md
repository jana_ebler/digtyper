# DIGTYPER #

DIGTYPER (Duplication and Inversion GenoTYPER) is a program which can genotype inversions and tandem duplication from paired-end sequencing data. For a given bam file which contains sequencing data, and a file that contains the positions of the variants to be genotyped, it computes the likeliest genotype for each of these variants using maximum likelihood estimation. Inversions are genotyped by considering *reversed reads* which are such with one end mapped inside the inversion breakpoints and the other end outside, and *split reads* which are such with one end stretching across one of the inversion breakpoints. For tandem duplications, genotyping is based on the insert size of the reads which here is defined as the length of the unsequenced segment between the two ends of a paired end read.
Therefore, mean and standard deviation of the insert size are needed and should be given to the program as options.
The usage of split read analysis for inversions is optional and can be skipped by using option *--only_reversed_reads*. Before split read analysis is possible, those reads need to be mapped first, using clever toolkit as explained below (Note: this is not necessary if split read analysis is skipped!).

**output:** A vcf-file that contains the computed genotypes for each of the given variants.


## install DIGTYPER:


```

#!shell


git clone https://jana_ebler@bitbucket.org/jana_ebler/digtyper.git
cd digtyper
cmake . 
make
```

### required libraries: ###

    boost
    cmake
    zlib
    
    
## Alternatively: install DIGTYPER into conda environment: ##

```

#!shell


git clone https://jana_ebler@bitbucket.org/jana_ebler/digtyper.git
cd digtyper
conda env create -f digtyper.yml
conda activate digtyper
cmake . 
make
```



## usage: ##
For split read analysis the split reads need to be aligned first. For this step, clever toolkit (cdk) must be installed ([https://bitbucket.org/tobiasmarschall/clever-toolkit](Link URL)).
In order to align split reads, run the following command (in order to run genotyping without split reads, this
step can be skipped).

```
#!shell

./align_split_reads.sh <name of bam file > <input_seq.fa> <max_span>
```

"name_of_bam" is the name of the bam file without the ".bam" and "input_seq.fa" is the reference sequence that was used to map the reads in the bam file.
"max_span" is the maximal allowed span for a split read alignment. It should be
at least as long as the longest variant length of the variants to be genotyped.
**Note:** the output bam file will be called "<name of bam file>.sorted_laser.bam"


### genotyping: ###

Two input files are needed: A bam file containing the sequencing data and another file that contains the positions of the variants (inversions/tandem duplications) to be genotyped.
The latter file must be in the following format:




```
#!shell

<chr> <start_pos> <end_pos> <type>
```


Here, <type> can be "INV" or "DUP:TANDEM".
DIGTYPER is used as shown below. Note:  <name_of_bam_file> is the name of the bam-file used, i.e. without the ".bam". 

```
#!shell

./digtyper/src/DIGTYPER [options] <name of bam-file> <variations-file.txt>

Allowed options:
--only_reversed_reads                 use reversed reads in case of 
                                        inversions
  --only_split_reads                    use split reads in case of inversions
  -b [ --bam_window_size ] arg (=1000)  bam_window_size (number of bases to 
                                        look left and right of variant).
  -m [ --map_quality_threshold ] arg (=20)
                                        map quality threshold for the reads.
  -c [ --cov_threshold ] arg (=5)       coverage threshold.
  -g [ --gq_thres ] arg (=20)           gq threshold.
  -n [ --not_present_thres ] arg (=20)  not present threshold.
  -i [ --isize_mean ] arg (=500)        insert size mean
  -s [ --isize_stddev ] arg (=50)       insert size std deviation
  -r [ --read_end_len ] arg (=100)      length of read ends
  -o [ --outfile ] arg (=output_DIGTYPER.vcf)
                                        name of outfile

```

# **Note:**  
**genotyping inversions**: if split reads are not aligned (using *align_split_reads*), then use the following option for genotyping to skip split read analysis:
```
#!shell

--only_reversed_reads
```
 
 **genotyping duplications** : give mean and standard deviation of the insert size as options