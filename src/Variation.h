/* Copyright 2015 Jana Ebler
 *
 * This file is part of DIGTYPER.
 *
 * DIGTYPER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DIGTYPER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with genotyperINVDUP  If not, see <http://www.gnu.org/licenses/>.
 */
/* class represents a variation */

#ifndef VARIATION_H
#define VARIATION_H

#include <string>
#include <iostream>
#include <bamtools/api/BamAlignment.h>

class Variation
{
	public:
		typedef enum { NONE, INSERTION, DELETION, INVERSION, SNP, DUPTAN} variation_type_t;

		//default constructor
		Variation();

		//constructor
		Variation(std::string chrom, size_t coord1, size_t coord2, std::string& sequence, variation_type_t type);

		//returns the length of the variation
		int computeLength();

		//returns the center of the variation
		int computeCenter();

		//operator ==
		bool operator==(const Variation& v) const;

		bool isBetween(BamTools::BamAlignment& read);
		bool strechesOver(BamTools::BamAlignment& read, bool right);

		

		//the type
		variation_type_t type;

		//chromosome
		std::string chromosome;
		
		//leftmost coordinate
		size_t coord1;
	
		//rightmost coordinate
		size_t coord2;
		
		//sequence of the variation
		std::string sequence;
};

std::ostream& operator<<(std::ostream& output, Variation& v);


#endif // VARIATION_H



     

