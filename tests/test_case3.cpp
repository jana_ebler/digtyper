#include <boost/test/unit_test.hpp>
#include <iostream>
#include <bamtools/api/BamAux.h>
#include <bamtools/api/BamAlignment.h>
#include <bamtools/api/BamWriter.h>
#include <bamtools/api/BamReader.h>
#include <boost/program_options.hpp>
#include "../src/ReadBam.h"
#include "../src/ReadAlignment.h"
#include "../src/ReadVariations.h"
#include "../src/Variation.h"
#include <string>
#include <ctime>
#include <sstream>
#include <iomanip>
#include "../src/GenotypeDist.h"
#include "../src/Variation.h"
#include "../src/Genotyper.h"
#include <math.h>
#include <algorithm>

// Checks if all the correct reads are selected for short duplications

BOOST_AUTO_TEST_CASE( read_selection_shortDUP )
{
	std::string bamfile = "tests/testdata/test3";
	std::string variationsfile = "tests/testdata/input.txt";

	// open the bam file
	BamTools::BamReader bam_file;
	BOOST_CHECK_MESSAGE(bam_file.Open(bamfile.append(".bam")), "bamfile could not be opended.");
	BOOST_CHECK_MESSAGE(bam_file.OpenIndex(bamfile.append(".bai")), ".bai file could not be opended.");

	// open the variations file
	std::vector<Variation> variations;
	ReadVariations var_reader(variationsfile);
	BOOST_CHECK_MESSAGE(var_reader.read(variations), "variations file could not be opended.");
	BOOST_CHECK_MESSAGE(variations.size() == 4, "not all variants read.");
	
	// initialize Genotyper
	Genotyper genotyper(20,1000,true,true,false,15);
	Genotyper::variation_stats_t result;
	genotyper.compute_stats_dup_short(result, bam_file, variations[2], 140,256);

	// check if all reads are selected (according to IGV: 9 reads with one end inside, none with both)
	BOOST_CHECK_EQUAL(result.ref_support_insert+result.alt_support_insert,9);
}

