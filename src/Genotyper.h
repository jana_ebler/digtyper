/* Copyright 2015 Jana Ebler
 *
 * This file is part of DIGTYPER.
 *
 * DIGTYPER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DIGTYPER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with genotyperINVDUP  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GENOTYPER_H
#define GENOTYPER_H

#include <bamtools/api/BamReader.h>
#include <utility> 
#include "GenotypeDist.h"
#include "Variation.h"
#include "ReadAlignment.h"

class Genotyper
{
	public:
		typedef struct variation_stats_t
		{
			std::vector<GenotypeDist> read_probs;
			unsigned int ref_support_rev;
			unsigned int alt_support_rev;
			unsigned int ref_support_split;
			unsigned int alt_support_split;
			unsigned int ref_support_insert;
			unsigned int alt_support_insert;
			variation_stats_t() : ref_support_rev(0), alt_support_rev(0), ref_support_split(0), alt_support_split(0), ref_support_insert(0), alt_support_insert(0) {}
			int coverage() {return ref_support_rev + alt_support_rev + ref_support_split + alt_support_split+ref_support_insert+alt_support_insert;}
		
		} variation_stats_t;

		Genotyper(double map_quality_threshold, int bam_window_s, bool reversed, bool split, bool print_IV, int split_acc);

		//computes statistics for every read (and every genotype) according to the ml- function
		void compute_stats_inv(variation_stats_t& result, BamTools::BamReader& reader, Variation& var);

		//counts supporting reads (and neutral ones) for long duplications (> insert_size)
		void compute_stats_dup_long(variation_stats_t& result, BamTools::BamReader& reader, Variation& var, double isize_stddev, double isize_mean);

		//computes the probabiltiy for each genotype for short duplications (<= insert_size)
		void compute_stats_dup_short(variation_stats_t& result, BamTools::BamReader& reader, Variation& var, double isize_stddev, double isize_mean);

		// given the statistics computed by compute_stats compute the genotype using the ml-funtion (-> computes the product)
		GenotypeDist computeGenotype(Genotyper::variation_stats_t& result);
		
	private:

		//gets the positions from IV tag of the read alignment
		std::pair<int,int> getSplitPos(BamTools::BamAlignment& read);
		//computes the "real" left and right read ends based on their position
		void computeLeftAndRight(ReadAlignment& read, BamTools::BamAlignment& left, BamTools::BamAlignment& right);

		//threshold for mapping quality
		double map_quality_threshold;
		//size of the sequence area to consider
		int bam_window_size;
		bool use_reversed;
		bool use_split;
		bool print_IV;
		int split_acc;
		
};
#endif // GENOTYPER_H
