/* Copyright 2015 Jana Ebler
 *
 * This file is part of DIGTYPER.
 *
 * DIGTYPER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DIGTYPER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with genotyperINVDUP  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <bamtools/api/BamAux.h>
#include <bamtools/api/BamAlignment.h>
#include <bamtools/api/BamWriter.h>
#include <bamtools/api/BamReader.h>
#include <boost/program_options.hpp>
#include "ReadBam.h"
#include "ReadAlignment.h"
#include "ReadVariations.h"
#include "Variation.h"
#include <string>
#include <ctime>
#include <sstream>
#include <iomanip>
#include "GenotypeDist.h"
#include "Variation.h"
#include "Genotyper.h"
#include <math.h>
#include <algorithm>

using namespace boost;
namespace po = boost::program_options;

void usage(const char* name, const po::options_description& options_desc)
{
	std::cerr << "Usage: " << name << "<name of bam-file> <variantions-file.txt> [options]" << std::endl << std::endl;
	std::cerr << "variations-file.txt must be in following format:" << std::endl;
	std::cerr << "<chr> <start_pos> <end_pos> <type of variant (INV or DUP:TANDEM)>" << std::endl << std::endl;
	std::cerr << options_desc << std::endl;
}

//stores all information that is needed for vcf file (per variation)
typedef struct output_info_t{

	//the variation considered
	Variation variation;

	//the likeliest genotype
	GenotypeDist::genotype_t genotype;
	
	//result of ml-function for this variation
	GenotypeDist genotype_dist;			

} output_info_t;

std::string get_date_string() {
	time_t t = time(0);
	struct tm* now = localtime(&t);
	std::ostringstream oss;
	oss << (now->tm_year+1900) << std::setfill('0') << std::setw(2) << (now->tm_mon+1) << std::setw(2) << now->tm_mday;
	return std::string(oss.str());
}

double phred(double prob)
{
	return (int)round(std::min(255.0,-10*log10(prob)));
}

//input: bamfile-name, variation-filename, bam_window_size, map_quality threshold

int main(int argc, char* argv[]){

	bool use_splits = true;
	bool use_reversed_reads = true;
	bool print_IV = false;

	int bam_window_size;
	double map_quality_threshold;

	int cov_thres;
	int gq_thres;
	int not_present_thres;
	int isize_mean;
	int isize_stddev;
	int read_end_len;
	int split_acc;
	std::string out_name;

	po::options_description options_desc("Allowed options");
  	options_desc.add_options()
	("only_reversed_reads", "use reversed reads in case of inversions")
	("only_split_reads", "use split reads in case of inversions")
	("output_IV_tags", "prints the split read breakpoints (stored in IV-tags). Useful to check accuracy of variant breakpoints.")
    	("bam_window_size,b", po::value<int>(&bam_window_size)->default_value(1000), "bam_window_size (number of bases to look left and right of variant).")
	("map_quality_threshold,m",po::value<double>(&map_quality_threshold)->default_value(20), "map quality threshold for the reads.")
	("cov_threshold,c",po::value<int>(&cov_thres)->default_value(5), "coverage threshold.")
	("gq_thres,g",po::value<int>(&gq_thres)->default_value(20), "gq threshold.")
	("not_present_thres,n",po::value<int>(&not_present_thres)->default_value(20), "not present threshold.")
	("isize_mean,i", po::value<int>(&isize_mean)->default_value(500), "insert size mean")
	("isize_stddev,s", po::value<int>(&isize_stddev)->default_value(50), "insert size std deviation")
	("read_end_len,r", po::value<int>(&read_end_len)->default_value(100), "length of read ends")
	("outfile,o", po::value<std::string>(&out_name)->default_value("output_DIGTYPER.vcf"), "name of outfile")
	("split_accuracy,a,", po::value<int>(&split_acc)->default_value(15), "accuracy threshold for split reads, i.e. how much IV tag coord. is allowed to deviate from variant breakpoint.")
  	;

	//check if enough commandline parameters given
	if(argc < 3)
	{
		//throw std::runtime_error("too less commandline parameters given");
		usage(argv[0], options_desc);
		return 1;
	}

	//read commandline parameters
	std::string bamfile = argv[1];
	std::string variationfile = argv[2];

	po::variables_map options;
	try {
		po::store(po::parse_command_line(argc, argv, options_desc), options);
		po::notify(options);
	} catch(std::exception& e) {
		std::cerr << "error: " << e.what() << "\n";
		std::cerr << options_desc << std::endl;
		return 1;
	}

	if(options.count("only_reversed_reads")) {
 		use_reversed_reads = true;
		use_splits = false;
	}

	if(options.count("only_split_reads")) {
 		use_reversed_reads = false;
		use_splits = true;
	}

	if(options.count("output_IV_tags")){
		print_IV = true;
	}

	//open bam reader
	BamTools::BamReader bam_reader;
	if(!bam_reader.Open(bamfile.append(".bam")))
	{
		std::cerr << "bam-file could not be opened." << std::endl;
		return 1;
	};

	//load index
	if(!bam_reader.OpenIndex(bamfile.append(".bai")))
	{
		std::cerr << "could not open index file " << bamfile << ".bai" << std::endl;
		return 1;
	}

	//read variations from file
	std::vector<Variation> variations;

	ReadVariations var_reader(variationfile);
	if(!var_reader.read(variations))
	{
		std::cerr << "variations-file could not be read" << std::endl;
		return 1;
	}
	
	//write header of output vcf-file
	std::ofstream out(out_name.c_str());
	out << "##fileformat=VCFv4.2" << std::endl;
	out << "##fileDate=" << get_date_string() << std::endl;
	out << "##INFO=<ID=SVLEN,Number=1,Type=Integer,Description=\"length of the variant\">" << std::endl;
	out << "##INFO=<ID=SVTYPE,Number=1,Type=String,Description=\"Type of structural variant\">" << std::endl;
	out << "##INFO=<ID=END,Number=1,Type=Integer,Description=\"End position of the structural variant\">" << std::endl;	
	out << "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">" << std::endl;
	out << "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype quality: probability that the genotype is wrong (phred based).\">" << std::endl;
	out << "##FORMAT=<ID=L,Number=.,Type=Integer,Description=\"genotype likelihoods for absent, heterozygous, homozygous.\">" << std::endl;
	out << "##FORMAT=<ID=IE,Number=.,Type=Integer,Description=\"reversed read evidence in two comma-separated values in case of inversions: REF support, ALT support\">" << std::endl;
	out << "##FORMAT=<ID=SE,Number=.,Type=Integer,Description=\"Split-read evidence in two comma-separated values in case of inversions: REF support, ALT support\">" << std::endl;
	out << "##FORMAT=<ID=DE,Number=.,Type=Integer,Description=\"number of supporting and neutral reads in two comma-separated values in case of duplications: REF support, ALT support\">" << std::endl;
	out << "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tsample1" << std::endl;	

	//stores output information for each variation
	std::vector<output_info_t> to_output;

	//the used Genotyper
	Genotyper genotyper(map_quality_threshold,bam_window_size, use_reversed_reads, use_splits, print_IV, split_acc);
	
	// iteration over all variations, for each variation compute genotype statistics and finally the likeliest genotype
	for(size_t i = 0; i < variations.size(); i++)
	{
		std::string genotype = "./.";
		int gq = 0;
		int not_present = 0;
		std::string type = "NONE";

		int ref_support_rev = 0;
		int alt_support_rev = 0;
		int ref_support_split = 0;
		int alt_support_split = 0;
		int alt_support_dup = 0;
		int neutral_dup = 0;
		
		output_info_t o;

		switch(variations[i].type)
		{
			case Variation::INVERSION:
				{
					Genotyper::variation_stats_t result;
				
					//compute stats and finally compute genotype 
					genotyper.compute_stats_inv(result, bam_reader, variations[i]);
					o.variation = variations[i];

					//result.size() could be 0 (e.g. if no reads cover position)
					if(result.read_probs.size() != 0) 
					{	
				
						o.genotype_dist = genotyper.computeGenotype(result);
						o.genotype = o.genotype_dist.likeliestGenotype();

						//check coverage and not_present prob..., to find out which genotype to print (1/.)...
						gq = phred(o.genotype_dist.errorProb(o.genotype));
						not_present = phred(o.genotype_dist.getAbsent());

						if((result.coverage() >= cov_thres) && (gq >= gq_thres))
						{
							genotype = GenotypeDist::genoToString(o.genotype);
	
						} else if(not_present > not_present_thres){

							gq = not_present;
							genotype = "1/.";			
						}

						//parameters for vcf-output
						ref_support_rev = result.ref_support_rev;
						alt_support_rev = result.alt_support_rev;
						ref_support_split = result.ref_support_split;
						alt_support_split = result.alt_support_split;
						type = "INV";
					}else{
						if(!print_IV) std::cout << "no reads to be used for variation: " << variations[i].chromosome << " " << variations[i].coord1 << " - " << variations[i].coord2 << std::endl;
						type = "INV";
						genotype = "./.";
						o.genotype_dist = GenotypeDist();
					}
					break;
				}
			case Variation::DUPTAN: 
				{
					Genotyper::variation_stats_t result;
				
					//compute stats and compute genotype 
					if(variations[i].computeLength() < (isize_mean + read_end_len))
					{
						genotyper.compute_stats_dup_short(result, bam_reader, variations[i], isize_stddev, isize_mean);

					} else {

						genotyper.compute_stats_dup_long(result, bam_reader, variations[i], isize_stddev, isize_mean);
					}
					o.variation = variations[i];

					//result.size() could be 0 (e.g. if no reads cover position)
					if(result.read_probs.size() != 0) 
					{		

						o.genotype_dist = genotyper.computeGenotype(result);
						o.genotype = o.genotype_dist.likeliestGenotype();
	
						//check coverage and not_present prob..., to find out which genotype to print (1/.)...
						gq = phred(o.genotype_dist.errorProb(o.genotype));
						not_present = phred(o.genotype_dist.getAbsent());

						if((result.coverage() >= cov_thres) && (gq >= gq_thres))
						{
							genotype = GenotypeDist::genoToString(o.genotype);
	
						} else if(not_present > not_present_thres){

							gq = not_present;
							genotype = "1/.";			
						}
						
						alt_support_dup = result.alt_support_insert; 
						neutral_dup = result.ref_support_insert;
						type = "DUP:TANDEM";
					}else{
						if(!print_IV) std::cout << "no reads to be used for variation: " << variations[i].chromosome << " " << variations[i].coord1 << " - " << variations[i].coord2 << std::endl;
						genotype = "./.";
						type = "DUP:TANDEM";
						o.genotype_dist = GenotypeDist();
					}					
					break;
				}
			default: continue;
		}

		//write results in vcf file
		out << o.variation.chromosome; //CHROM
		out << '\t' << o.variation.coord1; //POS
		out << '\t' << "."; //ID
		out << '\t' << "N"; //REF
		out << '\t' << "<" << type << ">"; //ALT
		out << '\t' << "."; //QUAL
		out << '\t' << "PASS"; //FILTER
		out << '\t' << "SVTYPE=" << type << ";SVLEN=" << o.variation.computeLength() << ";END=" << o.variation.coord2; //INFO
		out << '\t' << "GT:GQ:L:IE:SE:DE"; //FORMAT
		out << '\t' << genotype; //Genotype
		out << ":" << gq; // prob that genotype is wrong
		out << ":" << o.genotype_dist; //Genotype probs	

		out << ":" << ref_support_rev << "," << alt_support_rev; // number of (reversed) reads that support rev,alt , 0 for other variations than inversions
		out << ":" << ref_support_split << "," << alt_support_split; // number of (split) reads that support rev,alt , 0 for other variations than inversions
		out << ":" << neutral_dup << "," << alt_support_dup << std::endl; // number of neutral and supporting reads in case of duplications
	
	}
	out.close();

	return 0;
}
