/* Copyright 2015 Jana Ebler
 *
 * This file is part of DIGTYPER.
 *
 * DIGTYPER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DIGTYPER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with genotyperINVDUP  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Variation.h"
#include <math.h>
#include <algorithm>

Variation::Variation()
	:type(Variation::NONE),
	 chromosome(""),
 	 coord1(0),
       	 coord2(0),
	 sequence("")
{}

		
Variation::Variation(std::string chrom, size_t coord1, size_t coord2, std::string& sequence, Variation::variation_type_t t)
	:type(t),
	 chromosome(chrom),
 	 coord1(coord1),
       	 coord2(coord2),
	 sequence(sequence)
{}

int Variation::computeLength()
{
	switch(type)
	{
		case Variation::DELETION: return (int)coord2 - (int)coord1;
		case Variation::INVERSION: return (int)coord2 - (int)coord1;
		case Variation::INSERTION: return (int)coord2;
		case Variation::DUPTAN: return (int)coord2 - (int)coord1;
		default: return 0;
	}
}		

int Variation::computeCenter()
{
	switch(type)
	{
		case Variation::DELETION: return ((int)coord2 + (int)coord1)/2;
		case Variation::INVERSION: return ((int)coord2 + (int)coord1)/2;
		case Variation::INSERTION: return (int)coord2;
		case Variation::DUPTAN: return ((int)coord2 + (int)coord1)/2;
		default: return 0;
	}
}

bool Variation::isBetween(BamTools::BamAlignment& read)
{
	return (coord1 <= read.Position) && (read.GetEndPosition() <= coord2);
}

bool Variation::strechesOver(BamTools::BamAlignment& read, bool right)
{

	if(right)
	{
		int dist1 = std::abs((int)read.Position-(int)coord2);
		int dist2 = std::abs((int)read.GetEndPosition()-(int)coord2);
		int mini = std::min(dist1,dist2);
		return (read.Position < coord2) && (coord2 < read.GetEndPosition()) && (mini > 35);
		//return (read.Position < coord2) && (coord2 < read.GetEndPosition()) && (mini > 0.15*read.Length);

	} else {

		int dist1 = std::abs((int)read.Position-(int)coord1);
		int dist2 = std::abs((int)read.GetEndPosition()-(int)coord1);
		int mini = std::min(dist1,dist2);
		return (read.Position < coord1) && (coord1 < read.GetEndPosition()) && (mini > 35);
		//return (read.Position < coord1) && (coord1 < read.GetEndPosition()) && (mini > 0.15*read.Length);
	}
}
		
bool Variation::operator==(const Variation& v) const {
	return (chromosome.compare(v.chromosome) == 0) && (coord1 == v.coord1) && (coord2 == v.coord2) && (type == v.type) && (sequence == v.sequence);
}

std::ostream& operator<<(std::ostream& output, Variation& v)
{
	std::string t = "";
	switch(v.type)
	{
		case Variation::DELETION: t = "DEL"; break;
		case Variation::INVERSION: t = "INV"; break;
		case Variation::INSERTION: t = "INS"; break;
		case Variation::SNP: t = "SNP"; break;
		case Variation::NONE: t = "NONE" ; break;
		case Variation::DUPTAN: t = "DUP:TANDEM"; break;
		default: break;
	}

	output << v.chromosome << " " << v.coord1 << " " << v.coord2 << " " << t << " " <<  v.sequence;

	return output;
}
