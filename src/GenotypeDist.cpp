/* Copyright 2015 Jana Ebler
 *
 * This file is part of DIGTYPER.
 *
 * DIGTYPER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DIGTYPER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with genotyperINVDUP  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GenotypeDist.h"
#include <iostream>

GenotypeDist::GenotypeDist()
	: genotype_prob(3, 1.0/3.0)
{}

GenotypeDist::GenotypeDist(long double absent, long double heterozygous, long double homozygous)
{
	genotype_prob.push_back(absent);
	genotype_prob.push_back(heterozygous);
	genotype_prob.push_back(homozygous);
}

void GenotypeDist::setDist(long double absent, long double heterozygous, long double homozygous)
{
	genotype_prob[0] = absent;
	genotype_prob[1] = heterozygous;
	genotype_prob[2] = homozygous;
}

GenotypeDist::genotype_t GenotypeDist::likeliestGenotype()
{
	unsigned int max_index = 0;
	double max_prob = genotype_prob[0];

	//search for highest probability
	for(unsigned int i = 1; i < 3; i++)
	{
		if(genotype_prob[i] > max_prob)
		{
			max_prob = genotype_prob[i];
			max_index = i;
		}
	}

	switch(max_index)
	{
		case 0: return GenotypeDist::ABSENT;
		case 1: return GenotypeDist::HETEROZYGOUS;
		default: return GenotypeDist::HOMOZYGOUS;
	} 
}

std::string GenotypeDist::genoToString(GenotypeDist::genotype_t geno)
{
	switch(geno)
	{
		case GenotypeDist::ABSENT: return "0/0";
		case GenotypeDist::HETEROZYGOUS: return "1/0";
		case GenotypeDist::HOMOZYGOUS: return "1/1";
		default: return " ";
	}
}

long double GenotypeDist::getAbsent()
{
	return genotype_prob[0];
}

long double GenotypeDist::getHeterozygous()
{
	return genotype_prob[1];
}

long double GenotypeDist::getHomozygous()
{
	return genotype_prob[2];
}

long double GenotypeDist::errorProb(GenotypeDist::genotype_t geno)
{
	long double result = 0.0;

	for(unsigned int i = 0; i < 3; i++)
	{
		if(geno == i) continue;
		result += genotype_prob[i];		
	}
	
	return result;
}


GenotypeDist operator*(GenotypeDist& a, GenotypeDist& b)
{
	GenotypeDist result = a;	
	long double s = 0.0;
	
	for(unsigned int i = 0; i < 3; i++)
	{
		result.genotype_prob[i] *= b.genotype_prob[i];
		s += result.genotype_prob[i];
	}	

	if(s == 0) s = 1;

	//normalize
	for(unsigned int i = 0; i < 3; i++)
	{
		result.genotype_prob[i] = result.genotype_prob[i]/s;
	}

	return result;
}

GenotypeDist operator+(GenotypeDist& a, GenotypeDist& b)
{
	GenotypeDist result = a;	
	long double s = 0.0;
	
	for(unsigned int i = 0; i < 3; i++)
	{
		result.genotype_prob[i] += b.genotype_prob[i];
		s += result.genotype_prob[i];
	}	

	if(s == 0) s = 1;

	//normalize
	for(unsigned int i = 0; i < 3; i++)
	{
		result.genotype_prob[i] = result.genotype_prob[i]/s;
	}

	return result;
}

std::ostream& operator<<(std::ostream& output, GenotypeDist dist)
{
	output << dist.genotype_prob[0] << "," << dist.genotype_prob[1] << "," << dist.genotype_prob[2];
	return output;
}
