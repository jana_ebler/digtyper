/* Copyright 2015 Jana Ebler
 *
 * This file is part of DIGTYPER.
 *
 * DIGTYPER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DIGTYPER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with genotyperINVDUP  If not, see <http://www.gnu.org/licenses/>.
 */
/* class to read a variations file */

#ifndef READVARIATIONS_H
#define READVARIATIONS_H

#include <string>
#include <vector>
#include "Variation.h"

class ReadVariations
{
	public:
		//constructor
		ReadVariations(std::string filename);

		//reads variations from the given file. returns true, if successful
		bool read(std::vector<Variation>& result);

	private:
		
		std::string filename;
};

#endif //READVARIATIONS_H
