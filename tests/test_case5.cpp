#include <boost/test/unit_test.hpp>
#include <iostream>
#include <bamtools/api/BamAux.h>
#include <bamtools/api/BamAlignment.h>
#include <bamtools/api/BamWriter.h>
#include <bamtools/api/BamReader.h>
#include <boost/program_options.hpp>
#include "../src/ReadBam.h"
#include "../src/ReadAlignment.h"
#include "../src/ReadVariations.h"
#include "../src/Variation.h"
#include <string>
#include <ctime>
#include <sstream>
#include <iomanip>
#include "../src/GenotypeDist.h"
#include "../src/Variation.h"
#include "../src/Genotyper.h"
#include <math.h>
#include <algorithm>

// Checks if all the correct reads are selected for long duplications

BOOST_AUTO_TEST_CASE( genotype_dist )
{
	GenotypeDist a(0.25,0.5,0.25);
	GenotypeDist b(0.3,0.5,0.2);

	GenotypeDist prod = a*b;
	
	long double first = 0.25*0.3;
	long double second = 0.5*0.5;
	long double third = 0.25*0.2;

	double sum = first+second+third;

	BOOST_CHECK_EQUAL(first/sum, prod.getAbsent());
	BOOST_CHECK_EQUAL(second/sum, prod.getHeterozygous());
	BOOST_CHECK_EQUAL(third/sum, prod.getHomozygous());
	
}

