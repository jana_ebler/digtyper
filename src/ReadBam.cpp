/* Copyright 2015 Jana Ebler
 *
 * This file is part of DIGTYPER.
 *
 * DIGTYPER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DIGTYPER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with genotyperINVDUP  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ReadBam.h"
#include <boost/unordered_map.hpp>


ReadBam::ReadBam()
{}

void ReadBam::readFromRegion(BamTools::BamReader& bam_reader,std::vector<ReadAlignment>& result, int ref_id, int start, int end)
{
	//read all alignments from the given region

	if(bam_reader.SetRegion(ref_id, start, ref_id, end))
	{
		boost::unordered_map<std::string, ReadAlignment> pairs;
		BamTools::BamAlignment aln;

		//find all alignments that cover the given region
		while(bam_reader.GetNextAlignment(aln))
		{	
			if(!aln.IsMapped()){continue;}
			if(!aln.IsPrimaryAlignment()) continue;
			

			//check if one of the reads is already stored
			boost::unordered_map<std::string, ReadAlignment>::iterator it = pairs.find(aln.Name);
			
			if(it != pairs.end())
			{
				
				if(aln.IsFirstMate()) 
				{
					(it->second).setLeft(aln);
				} else if(aln.IsSecondMate())
				{
					(it->second).setRight(aln);
				}

			} else {

				ReadAlignment r;
				r.setName(aln.Name);
				
				if(aln.IsFirstMate()){r.setLeft(aln);}
				else if(aln.IsSecondMate()) {r.setRight(aln);}
				pairs[aln.Name] = r;
			}
			
		}

		//now search for complete pair alignments (both ends mapped)
		for(boost::unordered_map<std::string, ReadAlignment>::iterator it = pairs.begin(); it != pairs.end(); ++it)
		{
			if((it->second.leftSet) && (it->second.rightSet))
			{
				result.push_back(it->second);
			}
		}

	} else {
	
		throw std::runtime_error("region could not be set");
	}
	
}
