/* Copyright 2015 Jana Ebler
 *
 * This file is part of DIGTYPER.
 *
 * DIGTYPER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DIGTYPER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with genotyperINVDUP  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ReadAlignment.h"

ReadAlignment::ReadAlignment()
	:leftSet(false),
	 rightSet(false)
{}

ReadAlignment::ReadAlignment(std::string name, BamTools::BamAlignment& aln1, BamTools::BamAlignment& aln2, bool leftSet, bool rightSet)
	:name(name),
	 left(aln1),
	 right(aln2),
	 leftSet(leftSet),
	 rightSet(rightSet)
{}

void ReadAlignment::setName(std::string n)
{
	name = n;
}

void ReadAlignment::setLeft(BamTools::BamAlignment& aln1)
{
	left = aln1;
	leftSet = true;
}

void ReadAlignment::setRight(BamTools::BamAlignment& aln2)
{
	right = aln2;
	rightSet = true;
}

std::string ReadAlignment::getName()
{
	return name;
}

BamTools::BamAlignment& ReadAlignment::getLeft()
{
	return left;
}

BamTools::BamAlignment& ReadAlignment::getRight()
{
	return right;
}
