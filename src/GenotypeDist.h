/* Copyright 2015 Jana Ebler
 *
 * This file is part of DIGTYPER.
 *
 * DIGTYPER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DIGTYPER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with genotyperINVDUP  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GENOTYPEDIST_H
#define GENOTYPEDIST_H

#include <vector>
#include <string>

class GenotypeDist
{
	public:
		typedef enum { ABSENT = 0, HETEROZYGOUS = 1, HOMOZYGOUS = 2 } genotype_t;

		//constructor
		GenotypeDist();

		GenotypeDist(long double absent, long double heterozygous, long double homozygous);

		//sets values for the genotype probabilities, assumes the prob.vector to be empty!!
		void setDist(long double absent, long double heterozygous, long double homozygous);

		//computes the genotype with highest probability
		genotype_t likeliestGenotype();

		//returns string representation of the given genotype
		static std::string genoToString(GenotypeDist::genotype_t geno);

		long double getAbsent();

		long double getHeterozygous();

		long double getHomozygous();

		long double errorProb(GenotypeDist::genotype_t geno);

		friend GenotypeDist operator*(GenotypeDist& a, GenotypeDist& b);

		friend GenotypeDist operator+(GenotypeDist& a, GenotypeDist& b);

		friend std::ostream& operator<<(std::ostream& output, GenotypeDist dist);

	private:

		//probabilities for all genotypes
		std::vector<long double> genotype_prob;
};

#endif //GENOTYPE_H
