/* Copyright 2015 Jana Ebler
 *
 * This file is part of DIGTYPER.
 *
 * DIGTYPER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DIGTYPER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with genotyperINVDUP  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef READBAM_H
#define READBAM_H

#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>
#include <bamtools/api/BamAux.h>
#include <bamtools/api/BamAlignment.h>
#include <bamtools/api/BamWriter.h>
#include <bamtools/api/BamReader.h>
#include "ReadAlignment.h"

//BamTools::BamAlignment

class ReadBam
{
	public:
		//constructor
		ReadBam();

		//extracts Reads from a given region, throws exception if something goes wrong!
		void readFromRegion(BamTools::BamReader& bam_reader,std::vector<ReadAlignment>& result, int ref_id, int start, int end);		
};

#endif //READBAM_H
