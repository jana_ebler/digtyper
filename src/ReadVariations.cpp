/* Copyright 2015 Jana Ebler
 *
 * This file is part of DIGTYPER.
 *
 * DIGTYPER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DIGTYPER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with genotyperINVDUP  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ReadVariations.h"
#include <iostream>
#include <fstream>

#include <boost/program_options.hpp>
#include <boost/unordered_set.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/math/distributions.hpp>

ReadVariations::ReadVariations(std::string file)
	:filename(file)
{}

bool ReadVariations::read(std::vector<Variation>& result)
{
	std::ifstream in(filename.c_str());
	
	if(in.fail())
	{
		std::cerr << "Could not open variations file" << std::endl;
		return false;
	}

	typedef boost::tokenizer<boost::char_separator<char> > tokenizer_t;
	boost::char_separator<char> whitespace_separator(" \t");
	std::string line;
	int linenr = 0;
	while (getline(in,line)) {
		linenr += 1;
		tokenizer_t tokenizer(line, whitespace_separator);
		std::vector<std::string> tokens(tokenizer.begin(), tokenizer.end());

		if(tokens.size() < 2)
		{
			std::cerr << "wrong file format in variations file" << std::endl;
			return false;
		}	

		std::string chromosome = tokens[0];
		std::string sequence = "";
		Variation::variation_type_t type;
		int coord1 = boost::lexical_cast<int>(tokens[1]);
		int coord2;

		//check if SNP
		if(tokens.size() <= 3)
		{
			result.push_back(Variation(chromosome,coord1, 0, sequence, Variation::SNP));
			continue;
		}

		// check for inv, ins, del, duptan
		if(tokens.size() >= 4)
		{
			coord2 = boost::lexical_cast<int>(tokens[2]);
			
			//check which variation
			if(tokens[3].compare("DEL") == 0)
			{
				result.push_back(Variation(chromosome, coord1, coord2, sequence, Variation::DELETION));
				continue;
			}

			//must be an inversion or insertion
			if(tokens[3].compare("INS") == 0)
			{
				type = Variation::INSERTION;

			} else if (tokens[3].compare("INV") == 0)
			{
				type = Variation::INVERSION;

			} else if (tokens[3].compare("DUP:TANDEM") == 0)
			{
				type = Variation::DUPTAN;

			} else {
				std::cerr << " no variation in line: " << linenr << std::endl;
				return false;
			}

			//check if sequence given
			if(tokens.size() >= 5)
			{
				sequence = tokens[4];
			}

			result.push_back(Variation(chromosome, coord1, coord2, sequence, type));
		}
	}

	return true;

}

